def minimumLength(txt, keys):
    answer = 10000000
    txt += " $"
    for i in xrange(len(txt) - 1):
        dup = list(keys)
        word = ""
        if i > 0 and txt[i - 1] != ' ':
            continue

        for j in xrange(i, len(txt)):
            if txt[j] == ' ':
                for k in xrange(len(dup)):
                    if dup[k] == word:
                        del(dup[k])
                        break
                word = ""
            else:
                word += txt[j]
            if not dup:
                answer = min(answer, j - i)
                break

    if(answer == 10000000):
        answer = -1

    return answer