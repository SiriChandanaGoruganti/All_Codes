import Control.Applicative
import Control.Monad
import System.IO
 
solve n
    | n == 0 = 1
    | n `mod` 2 == 1 = 2 * solve (n-1)
    | otherwise = 1 + solve (n - 1)
 
main :: IO ()
main = getLine >>= (\x -> replicateM_ (read x) $ getLine >>= (\y -> putStrLn . show . solve $ (read y)))