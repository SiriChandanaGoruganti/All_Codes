fib = 0:1:(zipWith (+) fib (tail fib))
x = 10^999
 
fib_1000 = length l
    where
      l = takeWhile (< x) fib
      
main = print fib_1000
