coin = [1,2,5,10,20,50,100,200]
 
countcoin 1 _ = 1
countcoin n x = sum $ map addcoin [0 .. x `div` coin !! pred n]
  where addcoin k = countcoin (pred n) (x - k * coin !! pred n)
 
Total = countcoin (length coin) 200

main = print Total
