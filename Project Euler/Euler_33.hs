import Data.Ratio
DCF = denominator $ product 
             [ a%c | a<-[1..9], b<-[1..9], c<-[1..9],
                     digit a b c, a /= b && a/= c]
   where digit a b c = ((10*a+b)%(10*b+c)) == (a%c)

main = do
    print DCF
