isPent n = (ax == 0) && ay `mod` 6 == 5
  where (ay, ax) = properFraction . sqrt $ 1 + 24 * (fromInteger n)
 
TPH = head [x | x <- scanl (+) 1 [5,9..], x > 40755, isPent x]

main = do
    print TPH
